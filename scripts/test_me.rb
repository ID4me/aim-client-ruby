#!/usr/bin/env ruby

require "bundler/setup"
require "id4me"
require "jwt"
require "httparty"

agent = Id4me::Agent.new(:email => 'tov@nrw.net',
                         :name => 'csltov9',
                         :full_name => 'Tobias Overkamp')

# agent.communicate('/agents',nil,nil)
#agent.create_key
id4me_key=agent.create_account
open('myID4MEkey.id', 'w') { |f|
  f.puts id4me_key
}
