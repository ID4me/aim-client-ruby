require "id4me/version"

# this need to be on top so that it can be used by 'required' classes
module Id4me
    def self.logger
      @logger ||= defined?(Rails) ? Rails.logger : Logger.new("log/logger.txt")
    end
end

require "id4me/agent"
require "id4me/configuration"
require "id4me/id_auth"

require 'thor'
require "id4me/c_l_i"

module Id4me

  class << self
    attr_writer :configuration
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configure
    yield(configuration)
  end


  class Error < StandardError; end
end

