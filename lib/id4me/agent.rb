require 'httparty'
require 'jwt'
require 'jose'
require "base64"
require "json"


# TODO make sure that key is not generated again and again
# also save the id
module Id4me

  class Agent


    @@base_uri = 'https://id.staging.denic.de/aim/v1'
    @@alg = 'RS512'

    include HTTParty
    base_uri @@base_uri

    # this is a nice feature: Log all stuff being sent to and received from HTTP into logfile.
    # so useful if you are trying to find out what's going on
    # TODO If you want to, you may use a configuration option for this... I don't
    # debug_output Id4me::logger


    attr_accessor :name
    attr_accessor :full_name
    attr_accessor :email
    attr_accessor :key_file_name

    attr_accessor :id4me_key
    attr_accessor :jwk

    def initialize(params = {})
      self.name = params[:name]
      self.full_name = params[:full_name]
      self.email = params[:email]
      self.key_file_name = params[:key_file_name]
    end

    def self.load(filename='key.json')
      json_dump = File.read(filename)
      ret = self.new
      ret.from_json!(json_dump)
      ret
    end

    def create_account
      Id4me.logger.debug self.to_json
      %w[name full_name email].each do |p|
        raise "need parameter #{p} to create an agent" if self.send(p).to_s =~ /^\s*$/
      end
      key = OpenSSL::PKey::RSA.new(1024)
      self.key_file_name ||= 'mykey'
      Id4me.logger.debug key.export #just for debugging
      open(key_file_name + '.pem', 'w') {|f|
        f.puts key.to_pem
      }
      @key = key
      jwk_temp = JOSE::JWK.from_key(key)
      self.jwk = get_jwk_json_payload_from_jwk_key(jwk_temp)

      id4me_key = send_http_request

      open(key_file_name + '.id', 'w') { |f|
        f.puts id4me_key
      }
      self.id4me_key=id4me_key
      Id4me.logger.debug self.to_json
      self.id4me_key
    end

    def to_json
      hash = {}
      self.instance_variables.each do |var|
        hash[var] = self.instance_variable_get var
      end
      hash.to_json
    end
    def from_json! string
      JSON.load(string).each do |var, val|
        self.instance_variable_set var, val
      end
    end

    private

    def send_http_request
      url = @@base_uri + '/agents'
      headers = {
          #    'kid': '', # key id will be provided by DENIC
          #    'b64': false, #works without this
          'method': 'POST',
          'jwk': self.jwk,
          'url': url,
          'use': 'sig'
      }
      id4me_key = post_jws_detached_data(url, payload, headers)
    end

    def payload
      {
          name: name,
          fullName: full_name,
          email: email
      }
    end

    def public_key_e
      @key_hash['e']
    end

    def public_key_n
      @key_hash['n']
    end


    def post_jws_detached_data(url, payload, headers, response_json = true)
      jwk = JWT::JWK.new(@key)
      token = JWT.encode(payload, jwk.keypair, @@alg, headers)

      parts = token.split('.')
      detached_signature = parts[0] + ".." + parts[2]
      protected_header = parts[1]

      additional_body = Base64.decode64(protected_header) #"unbase64 the header again. self.payload would work as well"

      result = self.class.post(url,
                               :body => additional_body,
                               :headers => {
                                   'Content-type' => 'application/json',
                                   'jws-detached-signature' => detached_signature}
      )

      Id4me.logger.debug result.to_yaml
      Id4me.logger.debug "Header"
      Id4me.logger.debug(result.header)
      Id4me.logger.debug "Body"
      Id4me.logger.debug(result.body)
      case result.code
      when 200, 201
        Id4me.logger.debug('success')
        Id4me.logger.debug('service has returned:')
        # result_hash = JSON.load result.body
        # seems result maps the elements into a hash by itself
        Id4me.logger.debug "Hey, your new ID is #{result['id']}"
        return result['id']
      when 401
        Id4me.logger.debug('O noes! Error 401! This failed')
        raise :result_code => 2001
      when 404
        Id4me.logger.debug('O noes not found! Error 404')
        raise :result_code => 2001
      when 500...600
        Id4me.logger.debug("ZOMG ERROR #{result.code}. Stop processing here")
        raise :result_code => 2600
      end
    end

    def get_jwk_json_payload_from_jwk_key(jwk_key)
      @key_hash = JSON.load jwk_key.to_binary
      Id4me.logger.debug "KEY-Object"
      Id4me.logger.debug @key_hash.to_yaml
      {
          'e': public_key_e, #NO base64 encoding...
          'kty': 'RSA',
          'n': public_key_n  #NO base64 encoding...
      }
    end
  end
end