module Id4me

  class CLI < Thor
    desc "hello world", "my first cli yay"

    desc "create-agent", "create a new agent"
    option :name, required: true, banner: "<Agent Name>"
    option :full_name, required: true, banner: "<Agent Full-Name>"
    option :email, required: true, banner: "<Agent Email>"
    option :filename, banner: "<Key-File-Name>"

    def create_agent
      agent = Id4me::Agent.new(name: options[:name], full_name: options[:full_name],
                               email: options[:email],
                               key_file_name: options[:filename])
      agent.create_account
    end
  end
end
