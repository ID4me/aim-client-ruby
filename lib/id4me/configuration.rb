module Id4me
  class Configuration
    attr_accessor :jwk_alg
    attr_accessor :api_url

    attr_accessor :jwk_id

    attr_accessor :jwk_pem #where can I find the PEM File. For obvious reasons better not in the repo


    def initialize
      @api_url = 'https://id.staging.denic.de/aim/v1'
      @jwk_alg = 'RS512'
    end
  end
end