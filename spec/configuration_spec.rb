RSpec.describe Id4me::Configuration do
  it "has a default url" do
    expect(Id4me.configuration.api_url).not_to be nil
  end
end
