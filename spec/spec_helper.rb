require "bundler/setup"
require 'webmock/rspec'

require "id4me"

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  # Fake requests to id4me provider to prevent creating of stuff again and again...
  config.before(:each) do
    stub_request(:any, /denic.de/).to_rack(FakeId4me)
  end
end
