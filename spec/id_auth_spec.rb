RSpec.describe Id4me::IdAuth do
  it "should be able to create a local auth identifier (authz) object" do
    expect(Id4me::IdAuth.new).not_to be nil
  end

  it "should be able to create the local auth identifier (authz) object at the registry" do
    id_auth = Id4me::IdAuth.new(:identifier => 'tov_nrw_net')
    expect(id_auth.create).not_to be == nil
  end
end
