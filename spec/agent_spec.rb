RSpec.describe Id4me::Agent do

  it "should be able to load an Agent from a json-file" do
    agent = Id4me::Agent.load
    expect(agent.id4me_key).not_to be nil
    expect(agent.id4me_key).to be == '5519c63f-722c-417a-a5d6-ba6716021069'
  end
end
